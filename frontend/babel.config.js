module.exports = {
    presets: [
      '@babel/preset-env', // Transpile modern JavaScript features
      '@babel/preset-react', // Transpile JSX syntax
    ],
    plugins: [
      // Babel plugins if needed
    ],
  };
  