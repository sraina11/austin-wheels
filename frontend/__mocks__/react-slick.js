// __mocks__/react-slick.js

const React = require('react');

const MockSlick = ({ children }) => {
  return <div data-testid="slick-slider">{children}</div>;
};

export default MockSlick;
