import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = "https://www.austin-wheels.me/"

class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        options = webdriver.ChromeOptions()
        options.add_experimental_option("excludeSwitches", ["enable-logging"])
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}

        cls.driver = webdriver.Chrome(
            options=options, service=Service(ChromeDriverManager().install())
        )
        cls.driver.get(URL)

    def test_About(self):
        # Find the "About" link in the navbar and click it
        about_link = self.driver.find_element(By.XPATH, "//*[@id='root']/div[1]/nav/ul/li[2]/a")
        about_link.click()
        # Wait 5 seconds
        wait = WebDriverWait(self.driver, 5)
        about_us_text = wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id='root']/div[2]/h1[1]")))
        # Check if "About us" is showing on the page
        self.assertTrue(about_us_text.text == 'About Us')
        wait = WebDriverWait(self.driver, 15)
    
    def test_ZipCodes(self):
        zip_link = self.driver.find_element(By.XPATH, "//*[@id='root']/div[1]/nav/ul/li[3]/a")
        zip_link.click()
        wait = WebDriverWait(self.driver, 5)
        zip_page = wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div[2]/p[1]")))
        self.assertTrue(zip_page.text == 'Zip Codes')
        wait = WebDriverWait(self.driver, 15)

    def test_Location(self):
        location_link = self.driver.find_element(By.XPATH, "//*[@id='root']/div[1]/nav/ul/li[4]/a")
        location_link.click()
        wait = WebDriverWait(self.driver, 5)
        location_page = wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div[2]/p[1]")))
        self.assertTrue(location_page.text == 'Locations')
        wait = WebDriverWait(self.driver, 15)

    def test_Location_more(self):
        transport_link = self.driver.find_element(By.XPATH, '/html/body/div/div[1]/nav/ul/li[4]/a')
        transport_link.click()
        wait = WebDriverWait(self.driver, 5)
        transport_page = wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div[2]/p[2]")))
        self.assertTrue("Displaying" in transport_page.text and "out of" in transport_page.text and "results" in transport_page.text)
        wait = WebDriverWait(self.driver, 15)


    def test_Tools_Used(self):
        about_link = self.driver.find_element(By.XPATH, "//*[@id='root']/div[1]/nav/ul/li[2]/a")
        about_link.click()
        wait = WebDriverWait(self.driver, 5)
        about_us_text = wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id='root']/div[2]/h2[3]")))
        self.assertTrue(about_us_text.text == 'Tools Used')
        wait = WebDriverWait(self.driver, 15)

    def test_Home(self):
        home_link = self.driver.find_element(By.XPATH, "//*[@id='root']/div[1]/nav/ul/li[1]/a")
        home_link.click()
        wait = WebDriverWait(self.driver, 5)
        home_link_test = wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id='root']/div[2]/div[1]/div/h1")))
        self.assertTrue(home_link_test.text == 'Welcome to AustinWheels')
        wait = WebDriverWait(self.driver, 15)

    def test_Learn_More(self):
        learn_link = self.driver.find_element(By.XPATH, "//*[@id='root']/div[2]/div[1]/div/a/button")
        learn_link.click()
        wait = WebDriverWait(self.driver, 5)
        learn_link_test = wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id='root']/div[2]/h1[1]")))
        self.assertTrue(learn_link_test.text == 'About Us')
        wait = WebDriverWait(self.driver, 15)

    def test_splash(self):
        home_link = self.driver.find_element(By.XPATH, "//*[@id='root']/div[1]/nav/ul/li[1]/a")
        home_link.click()
        wait = WebDriverWait(self.driver, 5)
        home_link_test = wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id='root']/div[2]/div[1]/div/h3")))
        self.assertTrue(home_link_test.text == 'Your Guide to Accessibility in the Greater Austin Area')
        wait = WebDriverWait(self.driver, 15)

    def test_Stats(self):
        about_link = self.driver.find_element(By.XPATH, "//*[@id='root']/div[1]/nav/ul/li[2]/a")
        about_link.click()
        wait = WebDriverWait(self.driver, 5)
        about_us_text = wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id='root']/div[2]/h2[2]")))
        self.assertTrue(about_us_text.text == 'Total Stats')
        wait = WebDriverWait(self.driver, 15)\
        
    def test_Postman(self):
        about_link = self.driver.find_element(By.XPATH, "//*[@id='root']/div[1]/nav/ul/li[2]/a")
        about_link.click()
        wait = WebDriverWait(self.driver, 5)
        about_us_text = wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div[2]/h2[4]")))
        self.assertTrue(about_us_text.text == 'Postman Documentation')
        wait = WebDriverWait(self.driver, 15)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == "__main__":
    unittest.main()
