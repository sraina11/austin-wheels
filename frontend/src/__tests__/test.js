/* Heavily inspired by March Madness Mayhem and Park Dex */
import {BrowserRouter} from "react-router-dom"
import React from 'react';
//import App from './App';

jest.mock('react-spinners', () => ({
    FadeLoader: 'FadeLoader',
}));
  
import About from "../pages/About.js";
import LandingPage from "../pages/LandingPage";
import Locations from "../pages/Locations";
import Stops from "../pages/Stops";
import ZipCodes from "../pages/ZipCodes";

import LocationInstance from '../pages/instancePages/LocationInstance';
import StopInstance from '../pages/instancePages/StopInstance';
import ZipInstance from '../pages/instancePages/ZipInstance';

import LocationCard from "../components/LocationCard";
import ZipcodeCard from "../components/ZipcodeCard";
import TransportationCard from "../components/TransportationCard";
import Navbar from "../components/Navbar";
import MiniCard from "../components/MiniCard";

describe('Main Pages', () => {
    test('About Page', () => {
        const tree = <BrowserRouter>renderer.create(<About />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Landing Page', () => {
        const tree = <BrowserRouter>renderer.create(<LandingPage />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Locations Page', () => {
        const tree = <BrowserRouter>renderer.create(<Locations />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Stops Page', () => {
        const tree = <BrowserRouter>renderer.create(<Stops />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Zipcodes Page', () => {
        const tree = <BrowserRouter>renderer.create(<ZipCodes />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })
})

describe('Instance Pages', () => {
    test('Location Instance', () => {
        const tree = <BrowserRouter>renderer.create(<LocationInstance />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Stop Instance', () => {
        const tree = <BrowserRouter>renderer.create(<StopInstance />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Zip Instance', () => {
        const tree = <BrowserRouter>renderer.create(<ZipInstance />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })
})

describe('Components', () => {

    test('Location Card', () => {
        const tree = <BrowserRouter>renderer.create(<LocationCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Zipcode Card', () => {
        const tree = <BrowserRouter>renderer.create(<ZipcodeCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Transportation Card', () => {
        const tree = <BrowserRouter>renderer.create(<TransportationCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Navigation Bar', () => {
        const tree = <BrowserRouter>renderer.create(<Navbar />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });
  
    test('Mini Card', () => {
        const tree = <BrowserRouter>renderer.create(<MiniCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    // test('Main App', () => {
    //     const tree = <BrowserRouter>renderer.create(<App />).toJSON()</BrowserRouter>
    //     expect(tree).toMatchSnapshot()
    // });
})
