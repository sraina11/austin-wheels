// jest.config.js
module.exports = {
    moduleNameMapper: {
        '\\.(css|less|scss)$': 'identity-obj-proxy',
        '\\.(jpg|jpeg|png|gif)$': 'jest-transform-stub',
        'react-spinners': 'jest-transform-stub',
        '^react-slick$': 'austin-wheels/frontend/__mocks__/react-slick.js',
        "/axios/": "axios/dist/node/axios.js",
        "\\.(jpg|jpeg|png|webp)$": "<rootDir>/__mocks__/fileMock.js",
        "\\.(css|less|sass|scss)$": "<rootDir>/__mocks__/styleMock.js",
        "\\.(gif|ttf|eot|svg)$": "<rootDir>/__mocks__/fileMock.js",
        "!": "<rootDir>/__mocks__/fileMock.js"
    },
    transform: {
        '^.+\\.js$': 'babel-jest',
    },
    transformIgnorePatterns: ['node_modules/(?!(axios)/)']
};
  