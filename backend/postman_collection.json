{
	"info": {
		"_postman_id": "5f8862fa-f368-48e5-a15e-ffb8aa68f987",
		"name": "Austin Wheels API Documentation",
		"description": "The Austin Wheels API provides API calls to get information about wheelchair-accessible locations and transportation options in the Greater Austin Area.\n\n## **Response Format**\n\n- The API returns request responses in JSON format. When an API request returns an error, it is sent in the JSON response as an error key.\n    \n\n## Authentication/Rate and usage limits\n\nNo Authentication or Rate Limit",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json",
		"_exporter_id": "29992525",
		"_collection_link": "https://www.postman.com/solar-astronaut-334639/workspace/my-workspace/collection/29992579-5f8862fa-f368-48e5-a15e-ffb8aa68f987?action=share&source=collection_link&creator=29992525"
	},
	"item": [
		{
			"name": "Zip Codes",
			"item": [
				{
					"name": "zipcodes",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"",
									"pm.test(\"Response status code is 200\", function () {",
									"  pm.expect(pm.response.code).to.equal(200);",
									"});",
									"",
									"",
									"pm.test(\"Response is an array with at least one element\", function () {",
									"  const responseData = pm.response.json();",
									"  ",
									"  pm.expect(responseData).to.be.an('array');",
									"  pm.expect(responseData).to.have.lengthOf.at.least(1);",
									"});",
									"",
									"",
									"pm.test(\"Latitude and longitude values are within valid ranges\", function () {",
									"    const responseData = pm.response.json();",
									"",
									"    responseData.forEach(function (zipcode) {",
									"        pm.expect(zipcode.latitude).to.be.a('number').within(-90, 90, \"Latitude should be between -90 and 90\");",
									"        pm.expect(zipcode.longitude).to.be.a('number').within(-180, 180, \"Longitude should be between -180 and 180\");",
									"    });",
									"});",
									"",
									"",
									"pm.test(\"Postal code is a non-empty string\", function () {",
									"    const responseData = pm.response.json();",
									"    ",
									"    pm.expect(responseData).to.be.an('array');",
									"    responseData.forEach(function(data) {",
									"        pm.expect(data.postal_code).to.be.a('string').and.to.have.lengthOf.at.least(1, \"Value should not be empty\");",
									"    });",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "https://api.austin-wheels.me/zipcodes",
							"protocol": "https",
							"host": [
								"api",
								"austin-wheels",
								"me"
							],
							"path": [
								"zipcodes"
							],
							"query": [
								{
									"key": "county",
									"value": "Travis",
									"description": "Filters data to only show Zip Codes in specific county",
									"disabled": true
								},
								{
									"key": "sort",
									"value": "sort_val",
									"description": "Sorts data by sort_val (Population, # of locations, # of stops)",
									"disabled": true
								},
								{
									"key": "latitude",
									"value": "30.1388",
									"description": "Filters data to only show Zip Codes with specific size in mi^2",
									"disabled": true
								},
								{
									"key": "longitude",
									"value": "-97.2921",
									"disabled": true
								}
							]
						},
						"description": "Gets information about the authenticated user."
					},
					"response": [
						{
							"name": "zipcodes",
							"originalRequest": {
								"method": "GET",
								"header": [],
								"url": {
									"raw": "https://austin-wheels.me/api/zipcodes?city=Round_Rock",
									"protocol": "https",
									"host": [
										"austin-wheels",
										"me"
									],
									"path": [
										"api",
										"zipcodes"
									],
									"query": [
										{
											"key": "city",
											"value": "Round_Rock",
											"description": "Filter data to only show Zip Codes based in specified city"
										},
										{
											"key": "population",
											"value": "10,000",
											"description": "Filters data to only show Zip Codes with certain population",
											"disabled": true
										},
										{
											"key": "county",
											"value": "Travis",
											"description": "Filters data to only show Zip Codes in specific county",
											"disabled": true
										},
										{
											"key": "sort",
											"value": "sort_val",
											"description": "Sorts data by sort_val (Population, # of locations, # of stops)",
											"disabled": true
										},
										{
											"key": "area",
											"value": "300",
											"description": "Filters data to only show Zip Codes with specific size in mi^2",
											"disabled": true
										}
									]
								}
							},
							"_postman_previewlanguage": "json",
							"header": [
								{
									"key": "Content-Type",
									"value": "application/json",
									"name": "Content-Type",
									"description": "",
									"type": "text"
								}
							],
							"cookie": [],
							"body": "{\r\n    \"zip codes\": [\r\n        {\r\n            \"zipCode\": \"78664\",\r\n            \"city\": \"Round Rock\",\r\n            \"population\": \"123,876\",\r\n            \"county\": \"Williamson\",\r\n            \"landArea\": \"38 sq. miles\",\r\n            \"numLocations\": \"28\",\r\n            \"numStops\": \"10\"\r\n        } \r\n    ]\r\n}"
						}
					]
				},
				{
					"name": "zipcodes/<id>",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Response status code is 200\", function () {",
									"  pm.expect(pm.response.code).to.equal(200);",
									"});",
									"",
									"",
									"pm.test(\"Latitude is a valid number\", function () {",
									"  const responseData = pm.response.json();",
									"  ",
									"  pm.expect(responseData).to.be.an('object');",
									"  pm.expect(responseData.latitude).to.exist.and.to.be.a('number');",
									"});",
									"",
									"",
									"pm.test(\"Longitude should be a valid number\", function () {",
									"  const responseData = pm.response.json();",
									"  ",
									"  pm.expect(responseData).to.be.an('object');",
									"  pm.expect(responseData.longitude).to.exist.and.to.be.a('number');",
									"});",
									"",
									"",
									"pm.test(\"Location count is a non-negative integer\", function () {",
									"    const responseData = pm.response.json();",
									"    ",
									"    pm.expect(responseData.location_count).to.be.a('number');",
									"    pm.expect(responseData.location_count).to.be.at.least(0);",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "https://api.austin-wheels.me/zipcodes/:id",
							"protocol": "https",
							"host": [
								"api",
								"austin-wheels",
								"me"
							],
							"path": [
								"zipcodes",
								":id"
							],
							"query": [
								{
									"key": "zip_id",
									"value": "0",
									"description": "Zip ID 0 returns list of all avalible Zip IDs",
									"disabled": true
								}
							],
							"variable": [
								{
									"key": "id",
									"value": "1"
								}
							]
						}
					},
					"response": [
						{
							"name": "zipcodes/<id>",
							"originalRequest": {
								"method": "GET",
								"header": [],
								"url": {
									"raw": "https://austin-wheels.me/api/zipcodes/2?zip_id=78705",
									"protocol": "https",
									"host": [
										"austin-wheels",
										"me"
									],
									"path": [
										"api",
										"zipcodes",
										"2"
									],
									"query": [
										{
											"key": "zip_id",
											"value": "78705",
											"description": "Get data for a specific Zip Code id"
										},
										{
											"key": "zip_id",
											"value": "0",
											"description": "Zip ID 0 returns list of all avalible Zip IDs",
											"disabled": true
										}
									]
								}
							},
							"_postman_previewlanguage": "json",
							"header": [
								{
									"key": "Content-Type",
									"value": "application/json",
									"name": "Content-Type",
									"description": "",
									"type": "text"
								}
							],
							"cookie": [],
							"body": "{\n    \"zipCode\": \"78705\",\n    \"city\": \"Austin\",\n    \"population\": \"964,177\",\n    \"county\": \"Travis\",\n    \"landArea\": \"305.1 sq. miles\",\n    \"numLocations\": \"120\",\n    \"numStops\": \"185\"\n}"
						}
					]
				}
			],
			"description": "Get All Zip Codes, Zip Codes by City, or ZipCode by ID"
		},
		{
			"name": "Wheelchair Accessible Locations",
			"item": [
				{
					"name": "locations",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Response status code is 200\", function () {",
									"    pm.expect(pm.response.code).to.equal(200);",
									"});",
									"",
									"",
									"pm.test(\"Verify that the 'city' field is a non-empty string\", function () {",
									"  const responseData = pm.response.json();",
									"  ",
									"  pm.expect(responseData).to.be.an('array');",
									"  ",
									"  responseData.forEach(function(location) {",
									"    pm.expect(location.city).to.be.a('string').and.to.have.lengthOf.at.least(1, \"Value should not be empty\");",
									"  });",
									"});",
									"",
									"",
									"pm.test(\"The formatted_address field is a non-empty string\", function () {",
									"  const responseData = pm.response.json();",
									"",
									"  pm.expect(responseData).to.be.an('array');",
									"  ",
									"  responseData.forEach(function(location) {",
									"    pm.expect(location.formatted_address).to.be.a('string').and.to.have.lengthOf.at.least(1, \"Value should not be empty\");",
									"  });",
									"});",
									"",
									"",
									"pm.test(\"The name field is a non-empty string\", function () {",
									"    const responseData = pm.response.json();",
									"    ",
									"    pm.expect(responseData).to.be.an('array');",
									"    responseData.forEach(function(location){",
									"        pm.expect(location.name).to.be.a('string').and.to.have.lengthOf.at.least(1, \"Name should not be empty\");",
									"    });",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "https://api.austin-wheels.me/locations",
							"protocol": "https",
							"host": [
								"api",
								"austin-wheels",
								"me"
							],
							"path": [
								"locations"
							]
						}
					},
					"response": [
						{
							"name": "locations",
							"originalRequest": {
								"method": "GET",
								"header": [],
								"url": {
									"raw": "https://austin-wheels.me/api/locations?city=Austin",
									"protocol": "https",
									"host": [
										"austin-wheels",
										"me"
									],
									"path": [
										"api",
										"locations"
									],
									"query": [
										{
											"key": "zip",
											"value": "78705",
											"description": "Filter locations based on ZipCode",
											"disabled": true
										},
										{
											"key": "city",
											"value": "Austin",
											"description": "Filter locations based on City"
										},
										{
											"key": "type",
											"value": "Restaurant",
											"description": "Filter locations based on Type of Location",
											"disabled": true
										},
										{
											"key": "sort",
											"value": "sort_val",
											"description": "Sort data by sot_val (rating, time open)",
											"disabled": true
										},
										{
											"key": "days",
											"value": "Friday",
											"description": "Filter locations open on specific day",
											"disabled": true
										},
										{
											"key": "hour",
											"value": "2PM",
											"description": "Filter locations open at specific time",
											"disabled": true
										}
									]
								}
							},
							"_postman_previewlanguage": "json",
							"header": [
								{
									"key": "Content-Type",
									"value": "application/json",
									"name": "Content-Type",
									"description": "",
									"type": "text"
								}
							],
							"cookie": [],
							"body": "{\r\n    \"locations\": [\r\n        {\r\n            \"name\": \"Plume Salon\",\r\n            \"address\": \"5212 Avenue F, Austin, TX 78751\",\r\n            \"zipCode\": \"78751\",\r\n            \"city\": \"Austin\",\r\n            \"locationType\": \"Salon\",\r\n            \"rating\": \"Fully Accessible\",\r\n            \"daysOpen\": \"Monday - Saturday\",\r\n            \"backgroundImageUrl\": \"https://static1.squarespace.com/static/5ea709dd5538b6660db02dc3/t/6403f8e4fe7b516adeb484b2/1691438438576/\",\r\n        }\r\n    ]\r\n}"
						}
					]
				},
				{
					"name": "locations<id>",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Response status code is 200\", function () {",
									"  pm.response.to.have.status(200);",
									"});",
									"",
									"",
									"pm.test(\"Validate the first_photo object\", function () {",
									"    const responseData = pm.response.json();",
									"    ",
									"    pm.expect(responseData.first_photo).to.exist.and.to.be.an('object');",
									"    pm.expect(responseData.first_photo.height).to.exist.and.to.be.a('number');",
									"    pm.expect(responseData.first_photo.photo_reference).to.exist.and.to.be.a('string');",
									"    pm.expect(responseData.first_photo.width).to.exist.and.to.be.a('number');",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "https://api.austin-wheels.me/locations/:id",
							"protocol": "https",
							"host": [
								"api",
								"austin-wheels",
								"me"
							],
							"path": [
								"locations",
								":id"
							],
							"variable": [
								{
									"key": "id",
									"value": "1"
								}
							]
						}
					},
					"response": [
						{
							"name": "locations<id>",
							"originalRequest": {
								"method": "GET",
								"header": [],
								"url": {
									"raw": "https://austin-wheels.me/api/locations/3?loc_id=23&loc_id=0",
									"protocol": "https",
									"host": [
										"austin-wheels",
										"me"
									],
									"path": [
										"api",
										"locations",
										"3"
									],
									"query": [
										{
											"key": "loc_id",
											"value": "23",
											"description": "Get data for specific location ID"
										},
										{
											"key": "loc_id",
											"value": "0",
											"description": "Loc ID 0 returns list of all Location IDs"
										}
									]
								}
							},
							"_postman_previewlanguage": "json",
							"header": [
								{
									"key": "Content-Type",
									"value": "application/json",
									"name": "Content-Type",
									"description": "",
									"type": "text"
								}
							],
							"cookie": [],
							"body": "{\r\n    \"name\": \"Madam Mam's Thai Cuisine\",\r\n    \"address\": \"510 W 26th St, Austin, TX 78705\",\r\n    \"zipCode\": \"78705\",\r\n    \"city\": \"Austin\",\r\n    \"locationType\": \"Restaurant\",\r\n    \"rating\": \"Fully Accessible\",\r\n    \"daysOpen\": \"Monday - Saturday\",\r\n    \"backgroundImageUrl\": \"https://lostinaustin.org/wp-content/uploads/2019/06/blog-lostinaistin-best-of-west-campus-madam-mams.jpg\",\r\n } "
						}
					]
				}
			],
			"description": "Get All Locations, Locations by Rating, or Locations by ID"
		},
		{
			"name": "Wheelchair friendly transportation stops",
			"item": [
				{
					"name": "transport",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Response status code is 200\", function () {",
									"    pm.expect(pm.response.code).to.equal(200);",
									"});",
									"",
									"",
									"pm.test(\"The city field is a non-empty string\", function () {",
									"  const responseData = pm.response.json();",
									"  ",
									"  pm.expect(responseData).to.be.an('array');",
									"  responseData.forEach(function(item) {",
									"    pm.expect(item.city).to.be.a('string').and.to.have.lengthOf.at.least(1, \"City field should not be empty\");",
									"  });",
									"});",
									"",
									"",
									"pm.test(\"The formatted_address field is a non-empty string\", function () {",
									"  const responseData = pm.response.json();",
									"",
									"  pm.expect(responseData).to.be.an('array');",
									"  ",
									"  responseData.forEach(function(item) {",
									"    pm.expect(item.formatted_address).to.be.a('string').and.to.have.lengthOf.at.least(1, \"Value should not be empty\");",
									"  });",
									"});",
									"",
									"",
									"pm.test(\"Verify that the 'id' field is a non-negative integer\", function () {",
									"    const responseData = pm.response.json();",
									"    ",
									"    pm.expect(responseData).to.be.an('array');",
									"    ",
									"    responseData.forEach(function (item) {",
									"        pm.expect(item.id).to.exist.and.to.be.a('number').and.to.be.at.least(0);",
									"    });",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "https://api.austin-wheels.me/transport",
							"protocol": "https",
							"host": [
								"api",
								"austin-wheels",
								"me"
							],
							"path": [
								"transport"
							]
						}
					},
					"response": [
						{
							"name": "stops",
							"originalRequest": {
								"method": "GET",
								"header": [],
								"url": {
									"raw": "https://austin-wheels.me/api/stops?city=Austin",
									"protocol": "https",
									"host": [
										"austin-wheels",
										"me"
									],
									"path": [
										"api",
										"stops"
									],
									"query": [
										{
											"key": "zip",
											"value": "78705",
											"description": "Filter stop basped on ZipCode",
											"disabled": true
										},
										{
											"key": "city",
											"value": "Austin",
											"description": "Filter stop based on City"
										},
										{
											"key": "sort",
											"value": "sort_val",
											"description": "Sort data by sot_val (rating, time open, cost)",
											"disabled": true
										},
										{
											"key": "cost",
											"value": "5",
											"description": "Filter stops under a specific cost in USD",
											"disabled": true
										},
										{
											"key": "type",
											"value": "Bus",
											"description": "Filter stop based on Type (bus, car, train)",
											"disabled": true
										},
										{
											"key": "days",
											"value": "Friday",
											"description": "Filter stops open on specific day",
											"disabled": true
										},
										{
											"key": "hour",
											"value": "4PM",
											"description": "Filter stops open at specific time",
											"disabled": true
										}
									]
								}
							},
							"_postman_previewlanguage": "json",
							"header": [
								{
									"key": "Content-Type",
									"value": "application/json",
									"name": "Content-Type",
									"description": "",
									"type": "text"
								}
							],
							"cookie": [],
							"body": "{\r\n    \"stops\": [\r\n        {\r\n            \"name\": \"301 45th / Avenue C\",\r\n            \"address\": \"Corner of 301 45th St and Avenue C, Austin, TX 78751\",\r\n            \"zipCode\": \"78751\",\r\n            \"city\": \"Austin\",\r\n            \"locationType\": \"Bus Stop\",\r\n            \"rating\": \"Fully Accessible\",\r\n            \"daysOpen\": \"7 Days / Week\",\r\n            \"backgroundImageUrl\": \"https://lh5.googleusercontent.com/p/AF1QipNtnPnK9zRMjrGpkAKVFYgMHjFkfbCAXZd5yqya=w408-h544-k-no\",\r\n        }\r\n    ]\r\n\r\n}"
						}
					]
				},
				{
					"name": "transport/<id>",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"Response status code is 200\", function () {",
									"  pm.response.to.have.status(200);",
									"});",
									"",
									"",
									"pm.test(\"First photo has the required fields\", function () {",
									"    const responseData = pm.response.json();",
									"    ",
									"    pm.expect(responseData).to.be.an('object');",
									"    pm.expect(responseData.first_photo).to.exist.and.to.be.an('object');",
									"    pm.expect(responseData.first_photo.height).to.exist.and.to.be.a('number');",
									"    pm.expect(responseData.first_photo.photo_reference).to.exist.and.to.be.a('string');",
									"    pm.expect(responseData.first_photo.width).to.exist.and.to.be.a('number');",
									"});",
									"",
									"",
									"pm.test(\"Validate that the opening_hours_periods array is present and contains at least one element\", function () {",
									"    const responseData = pm.response.json();",
									"    ",
									"    pm.expect(responseData).to.be.an('object');",
									"    pm.expect(responseData.opening_hours_periods).to.exist.and.to.be.an('array').and.to.have.lengthOf.at.least(1, \"Value should contain at least one element\");",
									"});",
									"",
									"",
									"pm.test(\"The 'reviews' array is present and contains at least one element\", function () {",
									"    const responseData = pm.response.json();",
									"    ",
									"    pm.expect(responseData).to.be.an('object');",
									"    pm.expect(responseData.reviews).to.exist.and.to.be.an('array').with.lengthOf.at.least(1);",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "https://api.austin-wheels.me/transport/:id",
							"protocol": "https",
							"host": [
								"api",
								"austin-wheels",
								"me"
							],
							"path": [
								"transport",
								":id"
							],
							"variable": [
								{
									"key": "id",
									"value": "1"
								}
							]
						}
					},
					"response": [
						{
							"name": "stop<id>",
							"originalRequest": {
								"method": "GET",
								"header": [],
								"url": {
									"raw": "https://austin-wheels.me/apistops/4?stop_id=6",
									"protocol": "https",
									"host": [
										"austin-wheels",
										"me"
									],
									"path": [
										"apistops",
										"4"
									],
									"query": [
										{
											"key": "stop_id",
											"value": "6",
											"description": "Get data for specific stop ID"
										},
										{
											"key": "stop_id",
											"value": "0",
											"description": "Stop ID 0 returns list of all Stop IDs",
											"disabled": true
										}
									]
								}
							},
							"_postman_previewlanguage": "json",
							"header": [
								{
									"key": "Content-Type",
									"value": "application/json",
									"name": "Content-Type",
									"description": "",
									"type": "text"
								}
							],
							"cookie": [],
							"body": "{\r\n    \"name\": \"26th & Pearl\",\r\n    \"address\": \"908 W 26th St, Austin, TX 78705\",\r\n    \"zipCode\": \"78705\",\r\n    \"city\": \"Austin\",\r\n    \"locationType\": \"Bus Stop\",\r\n    \"rating\": \"Fully Accessible\",\r\n    \"daysOpen\": \"7 Days / Week\",\r\n    \"backgroundImageUrl\": \"https://streetviewpixels-pa.googleapis.com/v1/thumbnail?panoid=ZjnFJ8rmSi9Z7FdVWQwVkw&cb_client=search.gws-prod.gps&yaw=346.3627&pitch=0&thumbfov=100&w=520&h=175\",\r\n }"
						}
					]
				}
			],
			"description": "Get all Stops, Stops by Rating, Stops by ID"
		}
	],
	"event": [
		{
			"listen": "prerequest",
			"script": {
				"type": "text/javascript",
				"exec": [
					""
				]
			}
		},
		{
			"listen": "test",
			"script": {
				"type": "text/javascript",
				"exec": [
					""
				]
			}
		}
	],
	"variable": [
		{
			"key": "baseUrl",
			"value": "https://farming-simulator.pstmn.io"
		}
	]
}