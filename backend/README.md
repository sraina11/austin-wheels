# Statistics
- Zip Codes: 60
- Locations: 3400
- Transportation: 592

# Example Queries
- api.austin-wheels.me: Gives Info on all possible paths as well as searching/sorting options
- 
- /zipcodes?search=Travis&sort=location_count: Gives all zipcodes in Travis sorted by # of locations
- /zipcodes?search=Austin: Gives all zipcodes in Austin
- 
- /locations?sort=rating: Gives all locations sorted by rating
- /locations?search=hospital&sort=name: Gives all hospitals sorted by name
- /locations?search=worst%20experience: Gives all locations with 'worst experience' in review
- Meaningful searches for filtering locations: restaurant, tourist_attraction, doctor, lodging, 
- grocery_or_supermarket, home_goods_store
- 
- /transport?search=bus&sort=rating: Gives all bus options sorted by rating
- /transport?search=car_rental: Gives all car_rental options
- /transport?search=wait%20time: Gives all locations with 'wait time' in review
- Meaningful searches for filtering transport: bus_station, train_station, transit_station, car_dealer, 
- car_rental
