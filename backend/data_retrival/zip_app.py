import requests
import json
import time

def count_locations(zip_info, loc_info):
    # Create a dictionary to store the count for each zip code
    zip_counts = {}

    # Iterate over each entry in loc_info.json
    for entry in loc_info:
        # Extract the zip code from the entry
        zip_code = entry.get('zip')

        # Increment the count for the zip code in the dictionary
        zip_counts[zip_code] = zip_counts.get(zip_code, 0) + 1

    # Update zip_info.json with the location counts
    total_locations = 0
    for zip_code, coordinates_list in zip_info.items():

        # Get the count for the current zip code
        count = zip_counts.get(zip_code, 0)

        # Add the count to the coordinates_list
        coordinates_list[0]['location_count'] = count

        # Increment the total locations
        total_locations += count

    return zip_info, total_locations

def count_stops(zip_info, loc_info):
    # Create a dictionary to store the count for each zip code
    zip_counts = {}

    # Iterate over each entry in loc_info.json
    for entry in loc_info:
        # Extract the zip code from the entry
        zip_code = entry.get('zip')

        # Increment the count for the zip code in the dictionary
        zip_counts[zip_code] = zip_counts.get(zip_code, 0) + 1

    # Update zip_info.json with the location counts
    total_locations = 0
    for zip_code, coordinates_list in zip_info.items():

        # Get the count for the current zip code
        count = zip_counts.get(zip_code, 0)

        # Add the count to the coordinates_list
        coordinates_list[0]['stop_count'] = count

        # Increment the total locations
        total_locations += count

    return zip_info, total_locations

def main():
    # Load zip_info.json
    with open('zip_info.json', 'r') as json_file:
        zip_info = json.load(json_file)
    '''
    # Load loc_info.json
    with open('loc_info.json', 'r') as json_file:
        loc_info = json.load(json_file)
    '''
    # Load loc_info.json
    with open('stop_info.json', 'r') as json_file:
        stops_info = json.load(json_file)

    # Count locations and update zip_info.json
    #updated_zip_info, total_locations = count_locations(zip_info, loc_info)
    updated_zip_info, total_locations = count_stops(zip_info, stops_info)

    # Save the updated zip_info to a new JSON file
    with open('zip_info.json', 'w') as json_file:
        json.dump(updated_zip_info, json_file, indent=2)

    print("Stop counts have been added to zip_info.json.")
    print("Total Stops:", total_locations)

if __name__ == "__main__":
    main()


'''
headers = {
    "apikey": "API Key here"
}

# First set of ZIP codes
params1 = {
    "codes": "78602,78605,78611,78620,78621,78640,76574,78613,78630,78626,78627,78628,78633,78660,78691,78634,76537,78641,78645,78646,78642,78644,78653,78654,78657,78666,78667,78676,78610,78664,78665,78680,78681,78682,78683,73301,73344,78701,78702,78703,78704,78705,78708,78709,78710,78711,78712,78713,78714,78715,78716,78717,78718,78719,78720,78721,78722,78723,78724,78725",
    "country": "US"
}

response1 = requests.get('https://app.zipcodebase.com/api/v1/search', headers=headers, params=params1)
data1 = response1.json()

# Second set of ZIP codes
params2 = {
    "codes": "78726,78727,78728,78729,78730,78731,78732,78733,78734,78735,78736,78737,78738,78739,78741,78742,78744,78745,78746,78747,78748,78749,78750,78751,78752,78753,78754,78755,78756,78757,78758,78759,78760,78761,78762,78763,78764,78765,78766,78767,78768,78769,78772,78773,78774,78778,78779,78780,78781,78783,78785,78786,78788,78789,78798,78799",
    "country": "US"
}

response2 = requests.get('https://app.zipcodebase.com/api/v1/search', headers=headers, params=params2)
data2 = response2.json()

# Combine the results
combined_results = {**data1["results"], **data2["results"]}

# Save the combined information to a JSON file
with open('zip_info.json', 'w') as json_file:
    json.dump(combined_results, json_file, indent=2)

print("ZIP code information has been saved.")
'''


'''

while True:
    response = requests.get(base_url, params=params)
    data = response.json()

    # Check if there are results in the current page
    results = data.get('results', [])
    if results:
        # Extract and store only the place_ids
        all_place_ids.extend([place.get('place_id') for place in results])

    # Check if there is a next page token
    next_page_token = data.get('next_page_token')

    # Break the loop if there is no next page token
    if not next_page_token:
        break

    # Wait for a short time before making the next request (as per Google's guidelines)
    time.sleep(2)

    # Include the next page token in the params for the next request
    params['pagetoken'] = next_page_token
'''