import json

def remove_empty(data):
    unique_places = {}
    for zip_code, entries in data.items():
        for entry in entries:
            if entry.get('location_count', 0) > 0 and entry.get('stop_count', 0):
                if zip_code not in unique_places:
                    unique_places[zip_code] = []
                unique_places[zip_code].append(entry)
    print(len(unique_places))
    return unique_places

def main():
    # Load your JSON data
    with open('zip_info.json', 'r') as json_file:
        data = json.load(json_file)

    # Remove duplicates
    unique_data = remove_empty(data)

    # Save the updated data to a new JSON file
    with open('zip_info_unique.json', 'w') as json_file:
        json.dump(unique_data, json_file, indent=2)

if __name__ == "__main__":
    main()
