import unittest
import application

# Copied the code for tests from https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/back-end/tests.py
# Edited this code to work for our website

class Tests(unittest.TestCase):
    def setUp(self):
        application.application.config["TESTING"] = True
        self.client = application.application.test_client()


    def testGetAllZipCodes(self):
        with self.client:
            response = self.client.get("/zipcodes")
            self.assertEqual(response.status_code, 200)
            
            self.assertEqual(len(response.json), 60)

    def testGetAllLocations(self):
        with self.client:
            response = self.client.get("/locations")
            self.assertEqual(response.status_code, 200)
            
            self.assertEqual(len(response.json), 3400)

    def testGetAllStops(self):
        with self.client:
            response = self.client.get("/transport")
            self.assertEqual(response.status_code, 200)
            
            self.assertEqual(len(response.json), 592)
    def testGetFirstZip(self):
        with self.client:
            response = self.client.get("/zipcodes/1")
            self.assertEqual(response.status_code, 200)
            
            self.assertEqual(len(response.json), 16)

    def testGetFirstLocation(self):
        with self.client:
            response = self.client.get("/locations/1")
            self.assertEqual(response.status_code, 200)
            
            self.assertEqual(len(response.json), 12)

    def testGetFirstTransport(self):
        with self.client:
            response = self.client.get("/transport/1")
            self.assertEqual(response.status_code, 200)
            
            self.assertEqual(len(response.json), 14)
    def testSearchZip(self):
        with self.client:
            response = self.client.get("/zipcodes?search=78602")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json), 1)

    def testSearchLocation(self):
        with self.client:
            response = self.client.get("/locations?search=Subway")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json), 53)

    def testSearchTransport(self):
        with self.client:
            response = self.client.get("/transport?search=MobilityWorks")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json), 1)
    

    def testSortZip(self):
        with self.client:
            response = self.client.get("/zipcodes?sort=city")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json[0]["city"], "Austin")
            self.assertEqual(len(response.json), 60)
    
    def testSortLocation(self):
        with self.client:
            response = self.client.get("/locations?sort=name")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json[0]["name"], "1Starr Enterprises")
            self.assertEqual(len(response.json), 3400)
    
    def testSortTransport(self):
        with self.client:
            response = self.client.get("/transport?sort=rating")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json[0]["name"], "Ottoline Motorsport")
            self.assertEqual(len(response.json), 592)
            

   
if __name__ == "__main__":
    unittest.main()
