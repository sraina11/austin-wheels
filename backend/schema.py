from flask_marshmallow import Marshmallow
from models import Zip, Location, Transport
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

ma = Marshmallow()

class ZipSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Zip


class LocationSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Location


class TransportSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Transport


zip_schema = ZipSchema()
location_schema = LocationSchema()
transport_schema = TransportSchema()
